package com.example.demo.database.dao;

import com.example.demo.database.Connector;
import com.example.demo.database.mapper.BusinessMapper;
import com.example.demo.database.model.BusinessModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class BusinessDAO implements BaseDAO<BusinessModel> {

    @Autowired
    private Connector dbConnector;

    @Override
    public List<BusinessModel> getAll() {
        return dbConnector.getJdbi().withHandle(handle ->
                handle.createQuery("SELECT * FROM business")
                        .map(new BusinessMapper())
                        .list());
    }

    @Override
    public BusinessModel findById(int id) {
        return dbConnector.getJdbi().withHandle(handle ->
                handle.createQuery("SELECT * FROM business WHERE id=:id")
                        .bind("id", id)
                        .map(new BusinessMapper())
                        .findFirst().orElse(null));
    }

    public List<BusinessModel> findByCharacterId(int id) {
        return dbConnector.getJdbi().withHandle(handle ->
                handle.createQuery("SELECT * FROM business WHERE characterId=:id")
                    .bind("id", id)
                    .map(new BusinessMapper())
                    .list());
    }
}
