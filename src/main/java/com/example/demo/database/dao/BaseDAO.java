package com.example.demo.database.dao;

import com.example.demo.database.model.BaseModel;

import java.util.List;

public interface BaseDAO <T extends BaseModel> {
    List<T> getAll();

    T findById(int id);
}
