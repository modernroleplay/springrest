package com.example.demo.database.dao;

import com.example.demo.database.Connector;
import com.example.demo.database.mapper.BusinessPositionMapper;
import com.example.demo.database.model.BusinessPositionModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class BusinessPositionDAO implements BaseDAO<BusinessPositionModel>{

    @Autowired
    private Connector dbConnector;

    @Override
    public List<BusinessPositionModel> getAll() {
        return dbConnector.getJdbi().withHandle(handle ->
                handle.createQuery("SELECT * FROM businessposition")
                        .map(new BusinessPositionMapper())
                        .list());
    }

    @Override
    public BusinessPositionModel findById(int id) {
        return dbConnector.getJdbi().withHandle(handle ->
                handle.createQuery("SELECT * FROM business WHERE id=:id")
                        .bind("id", id)
                        .map(new BusinessPositionMapper())
                        .findFirst().orElse(null));
    }

    public List<BusinessPositionModel> findByBusinessId(int id) {
        return dbConnector.getJdbi().withHandle(handle ->
                handle.createQuery("SELECT * FROM businessposition WHERE BusinessId=:id")
                        .bind("id", id)
                        .map(new BusinessPositionMapper())
                        .list());
    }
}
