package com.example.demo.database.dao;

import com.example.demo.database.Connector;
import com.example.demo.database.mapper.CharacterMapper;
import com.example.demo.database.model.CharacterModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class CharacterDAO implements BaseDAO<CharacterModel> {

    @Autowired
    private Connector dbConnector;

    @Override
    public List<CharacterModel> getAll() {
        return dbConnector.getJdbi().withHandle(handle ->
                handle.createQuery("SELECT * FROM modrp.character")
                .map(new CharacterMapper())
                .list());
    }

    @Override
    public CharacterModel findById(int id) {
        return dbConnector.getJdbi().withHandle(handle ->
                handle.createQuery("SELECT * FROM modrp.character WHERE id=:id")
                        .bind("id", id)
                        .map(new CharacterMapper())
                        .findFirst().orElse(null));
    }
}
