package com.example.demo.database.model;

import java.util.Date;

public class BusinessModel implements BaseModel {

    private int id;

    private Date lastDelivery;

    private boolean locked;

    private float buyPrice;

    private int ownerCharacterId;

    private int resources;

    private int maxResources;

    private String title;

    private float banking;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getLastDelivery() {
        return lastDelivery;
    }

    public void setLastDelivery(Date lastDelivery) {
        this.lastDelivery = lastDelivery;
    }

    public boolean isLocked() {
        return locked;
    }

    public void setLocked(boolean locked) {
        this.locked = locked;
    }

    public float getBuyPrice() {
        return buyPrice;
    }

    public void setBuyPrice(float buyPrice) {
        this.buyPrice = buyPrice;
    }

    public int getOwnerCharacterId() {
        return ownerCharacterId;
    }

    public void setOwnerCharacterId(int ownerCharacterId) {
        this.ownerCharacterId = ownerCharacterId;
    }

    public int getResources() {
        return resources;
    }

    public void setResources(int resources) {
        this.resources = resources;
    }

    public int getMaxResources() {
        return maxResources;
    }

    public void setMaxResources(int maxResources) {
        this.maxResources = maxResources;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public float getBanking() {
        return banking;
    }

    public void setBanking(float banking) {
        this.banking = banking;
    }
}
