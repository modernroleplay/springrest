package com.example.demo.database.model;

import com.example.demo.extensions.TripleExt;

import javax.management.AttributeNotFoundException;

public class BusinessPositionModel implements BaseModel {
    private int id;

    private TripleExt<Float, Float, Float> position;

    private int type;

    private String infoText;

    private String typeName;

    private Long dimension;

    @Override
    public void setId(int id) {
        this.id = id;
    }


    public TripleExt<Float, Float, Float> getPosition() {
        return position;
    }

    public void setPosition(TripleExt<Float, Float, Float> position) {
        this.position = position;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) throws AttributeNotFoundException {
        this.type = type;
        typeName = resolveTypeString(type);
    }

    public String getInfoText() {
        return infoText;
    }

    public void setInfoText(String infoText) {
        this.infoText = infoText;
    }

    public Long getDimension() {
        return dimension;
    }

    public void setDimension(Long dimension) {
        this.dimension = dimension;
    }

    public void setTypeUnknown() {
        type = 0;
        try {
            typeName = resolveTypeString(type);
        } catch (AttributeNotFoundException e) {
            typeName = "Unknown";
        }
    }

    private String resolveTypeString(int typeId) throws AttributeNotFoundException {
        switch (typeId) {
            case 0:
                return "Unknown";
            case 101:
                return "Einkaufsladen";
            case 103:
                return "Waffenladen";
            case 104:
                return "Kleidungsladen";
            case 1001:
                return "Tankstelle";
            default:
                throw new AttributeNotFoundException(String.format("There is no typeid with %s", typeId));
        }
    }
}
