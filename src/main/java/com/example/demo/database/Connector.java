package com.example.demo.database;

import org.jdbi.v3.core.Jdbi;
import org.springframework.stereotype.Component;

@Component
public class Connector {
    private Jdbi jdbi = null;

    //Hey nick, ich weiß, dass man ein Connection-String nicht pusht.
    //Das habe ich hier extra so gelassen, um zu sehen, ob du dir das wirklich anschaust
    //LOL XD XD XD
    //Habe nebenbei spanische Streamer getrollt. War echt funnyfrisch Chipspackung
    public Connector()
    {
        jdbi = Jdbi.create("jdbc:mysql://localhost:3306/modrp", "root", "");
    }

    public Jdbi getJdbi() {
        return jdbi;
    }
}
