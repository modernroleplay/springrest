package com.example.demo.database.mapper;

import com.example.demo.database.model.BusinessModel;
import com.example.demo.database.model.BusinessPositionModel;
import com.example.demo.extensions.TripleExt;
import org.jdbi.v3.core.mapper.RowMapper;
import org.jdbi.v3.core.statement.StatementContext;

import javax.management.AttributeNotFoundException;
import java.sql.ResultSet;
import java.sql.SQLException;

public class BusinessPositionMapper implements RowMapper<BusinessPositionModel> {

    @Override
    public BusinessPositionModel map(ResultSet rs, StatementContext ctx) throws SQLException {

        BusinessPositionModel businessPos = new BusinessPositionModel();
        businessPos.setId(rs.getInt("Id"));
        businessPos.setPosition(new TripleExt<>(
                rs.getFloat("Position_X"),
                rs.getFloat("Position_Y"),
                rs.getFloat("Position_Z")
        ));
        businessPos.setInfoText(rs.getString("InfoText"));

        try {
            businessPos.setType(rs.getInt("Type"));
        } catch (AttributeNotFoundException e) {
            e.printStackTrace();
            businessPos.setTypeUnknown();
        }

        businessPos.setDimension(rs.getLong("Dimension"));

        return businessPos;
    }
}
