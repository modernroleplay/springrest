package com.example.demo.database.mapper;

import com.example.demo.database.model.CharacterModel;
import org.jdbi.v3.core.mapper.RowMapper;
import org.jdbi.v3.core.statement.StatementContext;

import java.sql.ResultSet;
import java.sql.SQLException;

public final class CharacterMapper implements RowMapper<CharacterModel> {

    @Override
    public CharacterModel map(ResultSet rs, StatementContext ctx) throws SQLException {
        CharacterModel character = new CharacterModel();
        character.setFirstname(rs.getString("Firstname"));
        character.setLastname(rs.getString("Lastname"));
        return character;
    }
}
