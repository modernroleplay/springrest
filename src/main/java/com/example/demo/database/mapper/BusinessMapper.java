package com.example.demo.database.mapper;

import com.example.demo.database.model.BusinessModel;
import org.jdbi.v3.core.mapper.RowMapper;
import org.jdbi.v3.core.statement.StatementContext;

import java.sql.ResultSet;
import java.sql.SQLException;


public final class BusinessMapper implements RowMapper<BusinessModel> {


    @Override
    public BusinessModel map(ResultSet rs, StatementContext ctx) throws SQLException {
        BusinessModel business = new BusinessModel();

        business.setId(rs.getInt("Id"));
        business.setLastDelivery(rs.getDate("LastDelivery"));
        business.setLocked(rs.getBoolean("Locked"));
        business.setBuyPrice(rs.getFloat("OriginalBuyingPrice"));
        business.setOwnerCharacterId(rs.getInt("CharacterId"));
        business.setResources(rs.getInt("Resources"));
        business.setMaxResources(rs.getInt(("MaxResources")));
        business.setTitle(rs.getString("Name"));
        business.setBanking(rs.getFloat("Bank"));

        return business;
    }
}
