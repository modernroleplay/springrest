package com.example.demo.extensions;

public final class TupleExt<A, B> {
    public final A a;
    public final B b;

    public TupleExt(A a, B b) {
        this.a = a;
        this.b = b;
    }
}
