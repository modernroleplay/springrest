package com.example.demo.controller;

import com.example.demo.controller.mapping.CountMapping;
import com.example.demo.database.dao.BusinessPositionDAO;
import com.example.demo.database.dao.CharacterDAO;
import com.example.demo.database.model.BusinessPositionModel;
import com.example.demo.database.model.CharacterModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
public class BusinessPositionController {
    @Autowired
    private BusinessPositionDAO dao;

    @RequestMapping("/business/positions")
    public Map<String, List<BusinessPositionModel>> getBusinessPositionsById(@RequestParam int id) {
        Map<String, List<BusinessPositionModel>> map = new HashMap<>();

        List<BusinessPositionModel> dbBusinessPositions = dao.findByBusinessId(id);

        map.put("entries", dbBusinessPositions);
        return map;
    }
}
