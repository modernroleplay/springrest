package com.example.demo.controller.mapping;

import jdk.jshell.spi.ExecutionControl;

public class CountMapping {

    private int count;

    public CountMapping(int size) {
        count = size;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    //@TODO: import JsonConverter library
    public String toJson() throws ExecutionControl.NotImplementedException {
        throw new ExecutionControl.NotImplementedException("import JsonConverter before using this function");
    }
}

