package com.example.demo.controller;

import com.example.demo.controller.mapping.CountMapping;
import com.example.demo.database.dao.CharacterDAO;
import com.example.demo.database.model.CharacterModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
public class CharacterController {

    @Autowired
    private CharacterDAO dao;

    @RequestMapping("/character")
    public Map<String, List<CharacterModel>> index() {

        Map<String, List<CharacterModel>> map = new HashMap<>();

        List<CharacterModel> dbCharacters = dao.getAll();
        map.put("entries", dbCharacters);
        return map;
    }


}
