package com.example.demo.controller;

import com.example.demo.controller.mapping.CountMapping;
import com.example.demo.database.dao.BusinessDAO;
import com.example.demo.database.model.BusinessModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
public class BusinessController {

    @Autowired
    private BusinessDAO dao;

    @RequestMapping("/business")
    public Map<String, List<BusinessModel>> index() {

        Map<String, List<BusinessModel>> map
                = new HashMap<>();

        List<BusinessModel> dbBusiness = dao.getAll();

        map.put("entries", dbBusiness);

        return map;
    }

    @RequestMapping("/business/find")
    public BusinessModel getById(@RequestParam int id) {
        BusinessModel targetBusiness = dao.findById(id);
        if (targetBusiness == null) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "there is no business with that id!");
        }
        return targetBusiness;
    }

    @RequestMapping("/business/owner")
    public Map<String, List<BusinessModel>> getByOwner(@RequestParam int id, @RequestParam boolean extend) {

        Map<String, List<BusinessModel>> map
                = new HashMap<>();

        List<BusinessModel> dbBusiness = dao.findByCharacterId(id);
        map.put("entries", dbBusiness);

        return map;
    }
}
